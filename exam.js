var express = require("express"),
    bodyParser = require('body-parser'),
    http = require('http');

const fileType = require('file-type');

var words = [];

var ord = [{"orden": "order1" ,"val": "NONE"}];

var app = express();

app.use(bodyParser.json());

app.post("/removeduplicatewords", function(req, res){

  var parts = req.body.palabras.split(',');

  for(var i = 0;i < parts.length;i++){
    if(i == 0){
      words.push(parts[i]);
    }else{
      var different = true;
      for(var j = 0; j < words.length; j++) {
        if(words[j] == parts[i]){
          different = false;
          break;
        }
      }
      if(different){
        words.push(parts[i]);
      }
    }
  }

  res.send(JSON.stringify(words));
});


app.post("/detectfiletype", function(req, res){
  console.log(req.body.url);

  const url = req.body.url;

  http.get(url, resa => {
    //console.log("connection");
    //console.log(result);
    resa.once('data', chunk => {
        resa.destroy();
        console.log(fileType(chunk));
    });
  });
res.send("OK");
});

app.get("/botorder/:order", function(req,res){
  console.log("GET: " + req.params.order);
  var found = false;
  var result;
  for(var i = 0; i < ord.length;i++){
    if(ord[i].orden == req.params.order){
      result = ord[i].val;
      found = true;
    }
  }
  if(found){
    res.send(result);
  }else{
    res.send("NONE");
  }
});

app.post("/botorder/:order", function(req,res){
  console.log(req.params);
  ord.push({orden: req.params.order, val: req.body.botorder});
  res.send("OK");
});

app.listen(8000);
